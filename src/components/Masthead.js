import React from 'react'

function Masthead() {
  return (
    <header class="masthead">
            <div class="container">
                <div class="masthead-subheading">Welcome To Our Studio!</div>
                <div class="masthead-heading text-uppercase">L. M. RYAS AMIN AKBAR</div>
                <a class="btn btn-primary btn-xl text-uppercase" href="#services">Tell Me More</a>
            </div>
    </header>
  )
}

export default Masthead